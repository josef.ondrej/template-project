FROM python:3

MAINTAINER Your Name "josef.ondrej@outlook.com"

RUN apt-get update -y

RUN mkdir /template-project
COPY ./ /template-project

RUN pip3 install -r /template-project/requirements.txt

ENV FLASK_APP=/template-project/template_project/web/app.py
EXPOSE 5000

ENTRYPOINT ["/bin/bash", "-c"]
CMD ["python3 -m flask run --host=0.0.0.0"]
