Template Project
================

This is a simple template project showing how to run a [Flask](https://flask.palletsprojects.com/en/1.1.x/) server using
 [Docker](https://www.docker.com/) image. 